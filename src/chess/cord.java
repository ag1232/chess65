package chess;
/** 
 * This class will be used to create coordinates which are given
 * through user inputs and when checking if a piece's movement
 * path is valid.
 * @author andrewgonzalez
 * @author danielrodriguez
 */
public class cord {

	public int x;
	public int y;
	
	public cord (int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public cord(){}
	
}
