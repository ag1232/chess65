package chess;

import java.util.ArrayList;
import java.util.Collections;

import chess.pieces.Bishop;
import chess.pieces.King;
import chess.pieces.Knight;
import chess.pieces.Pawn;
import chess.pieces.Queen;
import chess.pieces.Rook;
/**
 * @author andrewgonzalez
 * @author danielrodriguez
 */

public class Chess {
	static cord startCoordinate = new cord(0,0);
	static cord endCoordinate = new cord(0,0);
	static int drawCounter;// Used for detecting if a draw is accepted
	static String enPassant = null;
	static int lastX;
	static int lastY;
	static int oldKingX;
	static int oldKingY;
	static boolean checkMateBoolean = false;
	static String promotionG = null;
		
	/**The Following Lists are used for Check/Checkmate
	 *  whiteP & blackP are list of all current player pieces --> maninly used for their coordinate location(s)**/
	static ArrayList <Piece> whitePieces = new ArrayList<Piece>(16);
	static ArrayList <Piece> blackPieces = new ArrayList<Piece>(16);
	/**List of current possible king moves:**/
	static ArrayList <cord> whiteKingMoves = new ArrayList<cord>(8);
	static ArrayList <cord> blackKingMoves = new ArrayList<cord>(8);
	
	public static void main(String[] args) {
		String input; 
		boolean legalMove; //used to check if move/input is legal
		

		//creates board object and populates it then prints board 
		Board b1 = new Board();
		b1.populateBoard();
		populateList(b1);
		b1.printBoard();
		
		boolean start = true;//used to determine when game ends
		boolean turn = true; //used to determine who's turn it is
		while(start == true){//for-loop in charge of keeping the game running as long as start = True
			if(turn == true){
				if(whitePieces.get(4) == null){
					System.out.println("\n");
					System.out.println("Black wins");
					return;
				}
				updateKingMoves(b1);
				System.out.println("\n");
				if(check(turn,b1) == true){
					if(checkMateBoolean == true){
						System.out.println("Checkmate");
						System.out.println("Black wins");
						return;
					}
					else {
						System.out.println("Check");
					}
				}
				System.out.print("White's move: ");
				input = IO.readString();
				if (input.equals("g2 g3")){
					debug(1);
				}
				
				if(input.equals("resign")){ //checks for resign, draw, or draw?
					System.out.println("Black wins");
					//end game.
					return;
				}
				else if(input.equals("draw") && drawCounter == 1){ //checks for draw option
					//end game.
					return;
				}
				
				else{//else if the input is NOT a special case (no draw, resign, etc.) then do:
					legalMove = translateInput(input,startCoordinate,endCoordinate,drawCounter, b1);
					//If the move is illegal, it will prompt user
					if (legalMove == false){
						System.out.println("Illegal move, try again");
						continue;
					}
					//checks if the tile at the start coordinate is empty AKA another illegal input
					else if (b1.board[startCoordinate.x][startCoordinate.y].isEmpty() == true){
						System.out.println("Illegal move, try again");
						continue;
					}
					
					
					else if(legalMove == true){//once it is deemed a legal move then do the following checks:
						
						//checks to see if the player is moving the appropriate color
						if(b1.board[startCoordinate.x][startCoordinate.y].piece.player.color.equals("black")){
							System.out.println("Illegal move, try again");
							continue;
						}
						//Here is the actual check to make sure that same colors don't capture each other
						else if(b1.board[endCoordinate.x][endCoordinate.y].piece != null){
							if(b1.board[startCoordinate.x][startCoordinate.y].piece.player.color.equals(b1.board[endCoordinate.x][endCoordinate.y].piece.player.color)){
								System.out.println("Illegal move, try again");
								continue;
							}
							else{
								//check individual piece's path/movement to ensure move is legal
								if (b1.board[startCoordinate.x][startCoordinate.y].piece.validMovement(startCoordinate, endCoordinate, b1) == true){
									if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("King")){
										oldKingX = startCoordinate.x;
										oldKingY = startCoordinate.y;
										//whites turn thus:
										whitePieces.get(4).row = endCoordinate.x;
										whitePieces.get(4).col = endCoordinate.y;
										updateKingMoves(b1);
										if(check(turn, b1) == true){
											System.out.println("Illegal move, try again");
											checkMateBoolean = false;
									//if check is true, this will undo everything the moving of the king and say illegal:
											whitePieces.get(4).row = oldKingX;
											whitePieces.get(4).col = oldKingY;
											updateKingMoves(b1);
											continue;
										}
										
									}
									
									Player temp1 = new Player("brown");
									Pawn temp = new Pawn(temp1);
									if(b1.board[endCoordinate.x][endCoordinate.y].piece ==null) {
										//put temp pawn here
										
										b1.board[endCoordinate.x][endCoordinate.y].piece = temp;
									}
									updateKingMoves(b1);
									if(check(turn, b1) == true){
										System.out.println("Illegal move, try again");
										checkMateBoolean = false;
										continue;
									}
									promotion(b1,turn);
									movePiece(b1,turn);
									b1.printBoard();
									turn = false;//changes turn to false AKA makes it black's turn
								}
								else{//executes if everything is valid EXCEPT the start piece's movement  
									System.out.println("Illegal move, try again");
									continue;
								}
							}
							
						}
						else{ //This 'else' handles the cases where it's a simple move and no piece is being captured
							if (b1.board[startCoordinate.x][startCoordinate.y].piece.validMovement(startCoordinate, endCoordinate, b1) == true){
								/** For En Passant **/
								if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("Pawn")){
									Pawn ptemp = (Pawn) b1.board[startCoordinate.x][startCoordinate.y].piece;
									if(ptemp.enPassantBoolean == true){//check if the move is an En Passant move
									//if it is an En Passant move, then we have to do a final check that the last move made was the piece about to be captured
										if( (endCoordinate.x + 1) == lastX && (endCoordinate.y)  == lastY){
											enPassant = "white";
											//checkMoveAfter = true;
											if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("King")){
												oldKingX = startCoordinate.x;
												oldKingY = startCoordinate.y;
												//whites turn thus:
												whitePieces.get(4).row = endCoordinate.x;
												whitePieces.get(4).col = endCoordinate.y;
												updateKingMoves(b1);
												if(check(turn, b1) == true){
													System.out.println("Illegal move, try again");
													checkMateBoolean = false;
											//if check is true, this will undo everything the moving of the king and say illegal:
													whitePieces.get(4).row = oldKingX;
													whitePieces.get(4).col = oldKingY;
													updateKingMoves(b1);
													continue;
												}
												
											}
											Player temp1 = new Player("brown");
											Pawn temp = new Pawn(temp1);;
											if(b1.board[endCoordinate.x][endCoordinate.y].piece ==null) {
												//put temp pawn here
												//temp.player.color = "brown";
												b1.board[endCoordinate.x][endCoordinate.y].piece = temp;
											}
											updateKingMoves(b1);
											if(check(turn, b1) == true){
												System.out.println("Illegal move, try again");
												checkMateBoolean = false;
												continue;
											}
											promotion(b1,turn);
											movePiece(b1,turn);
											enPassant = null;
											b1.printBoard();
											turn = false; //black's turn
										}
										else { //Else if move was a En Passant BUT the last move was NOT moving the pawn about to be captured:
											System.out.println("Illegal move, try again");
											continue;
										}
									}
									else{ // Else if the piece being moved is a pawn but was NOT an En Passant move:
										//checkMoveAfter = true;
										if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("King")){
											oldKingX = startCoordinate.x;
											oldKingY = startCoordinate.y;
											//whites turn thus:
											whitePieces.get(4).row = endCoordinate.x;
											whitePieces.get(4).col = endCoordinate.y;
											updateKingMoves(b1);
											if(check(turn, b1) == true){
												System.out.println("Illegal move, try again");
												checkMateBoolean = false;
										//if check is true, this will undo everything the moving of the king and say illegal:
												whitePieces.get(4).row = oldKingX;
												whitePieces.get(4).col = oldKingY;
												updateKingMoves(b1);
												continue;
											}
											
										}
										Player temp1 = new Player("brown");
										Pawn temp = new Pawn(temp1);
										if(b1.board[endCoordinate.x][endCoordinate.y].piece ==null) {
											//put temp pawn here
											//temp.player.color = "brown";
											b1.board[endCoordinate.x][endCoordinate.y].piece = temp;
										}
										updateKingMoves(b1);
										if(check(turn, b1) == true){
											System.out.println("Illegal move, try again");
											checkMateBoolean = false;
											continue;
										}
										promotion(b1,turn);
										movePiece(b1,turn);
										b1.printBoard();
										turn = false;
									}
								}/**End of Enp Passant Check **/
								
								else{ //else if piece is not a pawn do:
									//checkMoveAfter = true;
									if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("King")){
										oldKingX = startCoordinate.x;
										oldKingY = startCoordinate.y;
										//whites turn thus:
										whitePieces.get(4).row = endCoordinate.x;
										whitePieces.get(4).col = endCoordinate.y;
										updateKingMoves(b1);
										if(check(turn, b1) == true){
											System.out.println("Illegal move, try again");
											checkMateBoolean = false;
									//if check is true, this will undo everything the moving of the king and say illegal:
											whitePieces.get(4).row = oldKingX;
											whitePieces.get(4).col = oldKingY;
											updateKingMoves(b1);
											continue;
										}
										
									}
									Player temp1 = new Player("brown");
									Pawn temp = new Pawn(temp1);
									if(b1.board[endCoordinate.x][endCoordinate.y].piece ==null) {
										//put temp pawn here
										//temp.player.color = "brown";
										b1.board[endCoordinate.x][endCoordinate.y].piece = temp;
									}
									updateKingMoves(b1);
									if(check(turn, b1) == true){
										System.out.println("Illegal move, try again");
										checkMateBoolean = false;
										continue;
									}
									//b1.board[endCoordinate.x][endCoordinate.y].piece = temp;
									promotion(b1,turn);
									movePiece(b1,turn);
									b1.printBoard();
									turn = false;//changes turn to false AKA makes it black's turn
								}
							}
							else{//if piece is trying to move in an invalid way e.g. a knight moving straight.
								System.out.println("Illegal move, try again");
								continue;
							}
						}
					}
				}
							
			}
			
			/** Black's turn:  AKA a copy of the same methods/checks used above in White's turn**/
			else{ //when turn = false:
				if(blackPieces.get(4) == null){
					System.out.println("\n");
					System.out.println("White wins");
					return;
				}
				updateKingMoves(b1);
				System.out.println("\n");
				if(check(turn,b1) == true){
					if(checkMateBoolean == true){
						System.out.println("Checkmate");
						System.out.println("White wins");
						return;
					}
					else {
						System.out.println("Check");
					}
				}
				
				System.out.print("Black's move: ");
				input = IO.readString();
				//checks for resign, draw, or draw?
				if(input.equals("resign")){
					System.out.println("White wins");
					//end game.
					return;
				}
				else if(input.equals("draw") && drawCounter == 1){
					//end game.
					return;
				}
				else{
					legalMove =  translateInput(input,startCoordinate,endCoordinate,drawCounter,b1);
					//If the move is illegal, it will prompt user
					if (legalMove == false){
						System.out.println("Illegal move, try again");
						continue;
					}
					//checks if the tile at the start coordinate is empty
					else if (b1.board[startCoordinate.x][startCoordinate.y].isEmpty() == true){
						System.out.println("Illegal move, try again");
						continue;
					}
					//checks if pieces belong to the same color/player
					else if(legalMove == true){
						
						//checks to see if the player is moving the appropriate color
						if(b1.board[startCoordinate.x][startCoordinate.y].piece.player.color.equals("white")){
							System.out.println("Illegal move, try again");
							continue;
						}
						//Here is the actual check to make sure no friendly fire is taking place.
						else if(b1.board[endCoordinate.x][endCoordinate.y].piece != null){
							if(b1.board[startCoordinate.x][startCoordinate.y].piece.player.color.equals(b1.board[endCoordinate.x][endCoordinate.y].piece.player.color)){
								System.out.println("Illegal move, try again");
							}
							else{
								if (b1.board[startCoordinate.x][startCoordinate.y].piece.validMovement(startCoordinate, endCoordinate, b1) == true){
									if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("King")){
										oldKingX = startCoordinate.x;
										oldKingY = startCoordinate.y;
										//blacks turn thus:
										blackPieces.get(4).row = endCoordinate.x;
										blackPieces.get(4).col = endCoordinate.y;
										updateKingMoves(b1);
										if(check(turn, b1) == true){
											System.out.println("Illegal move, try again");
											checkMateBoolean = false;
									//if check is true, this will undo everything the moving of the king and say illegal:
											blackPieces.get(4).row = oldKingX;
											blackPieces.get(4).col = oldKingY;
											updateKingMoves(b1);
											continue;
										}
										
									}
									Player temp1 = new Player("brown");
									Pawn temp = new Pawn(temp1);
									if(b1.board[endCoordinate.x][endCoordinate.y].piece ==null) {
										//put temp pawn here
										//temp.player.color = "brown";
										b1.board[endCoordinate.x][endCoordinate.y].piece = temp;
									}
									updateKingMoves(b1);
									if(check(turn, b1) == true){
										System.out.println("Illegal move, try again");
										checkMateBoolean = false;
										continue;
									}
									promotion(b1,turn);
									movePiece(b1,turn);
									b1.printBoard();
									turn = true;
								}
								else{
									System.out.println("Illegal move, try again");
									continue;
								}
								
							}
							
						}
						else{
							if (b1.board[startCoordinate.x][startCoordinate.y].piece.validMovement(startCoordinate, endCoordinate, b1) == true){
								
								/** For En Passant **/
								if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("Pawn")){
									Pawn ptemp = (Pawn) b1.board[startCoordinate.x][startCoordinate.y].piece;
									if(ptemp.enPassantBoolean == true){//check if the move is an En Passant move
									//if it is an En Passant move, then we have to do a final check that the last move made was the piece about to be captured
										if( (endCoordinate.x - 1) == lastX && (endCoordinate.y)  == lastY){
											enPassant = "black";
											if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("King")){
												oldKingX = startCoordinate.x;
												oldKingY = startCoordinate.y;
												//blacks turn thus:
												blackPieces.get(4).row = endCoordinate.x;
												blackPieces.get(4).col = endCoordinate.y;
												updateKingMoves(b1);
												if(check(turn, b1) == true){
													System.out.println("Illegal move, try again");
													checkMateBoolean = false;
											//if check is true, this will undo everything the moving of the king and say illegal:
													blackPieces.get(4).row = oldKingX;
													blackPieces.get(4).col = oldKingY;
													updateKingMoves(b1);
													continue;
												}
												
											}
											//checkMoveAfter ==true;
											Player temp1 = new Player("brown");
											Pawn temp = new Pawn(temp1);
											if(b1.board[endCoordinate.x][endCoordinate.y].piece ==null) {
												//put temp pawn here
												//temp.player.color = "brown";
												b1.board[endCoordinate.x][endCoordinate.y].piece = temp;
											}
											updateKingMoves(b1);
											if(check(turn, b1) == true){
												System.out.println("Illegal move, try again");
												checkMateBoolean = false;
												continue;
											}
											promotion(b1,turn);
											movePiece(b1,turn);
											enPassant = null;
											b1.printBoard();
											turn = true;
										}
										else { //Else if move was a En Passant BUT the last move was NOT moving the pawn about to be captured:
											System.out.println("Illegal move, try again");
											continue;
										}
									}
									else{ // Else if the piece being moved is a pawn but was NOT an En Passant move:
										if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("King")){
											oldKingX = startCoordinate.x;
											oldKingY = startCoordinate.y;
											//blacks turn thus:
											blackPieces.get(4).row = endCoordinate.x;
											blackPieces.get(4).col = endCoordinate.y;
											updateKingMoves(b1);
											if(check(turn, b1) == true){
												System.out.println("Illegal move, try again");
												checkMateBoolean = false;
										//if check is true, this will undo everything the moving of the king and say illegal:
												blackPieces.get(4).row = oldKingX;
												blackPieces.get(4).col = oldKingY;
												updateKingMoves(b1);
												continue;
											}
											
										}
										Player temp1 = new Player("brown");
										Pawn temp = new Pawn(temp1);
										if(b1.board[endCoordinate.x][endCoordinate.y].piece ==null) {
											//put temp pawn here
											//temp.player.color = "brown";
											b1.board[endCoordinate.x][endCoordinate.y].piece = temp;
										}
										updateKingMoves(b1);
										if(check(turn, b1) == true){
											System.out.println("Illegal move, try again");
											checkMateBoolean = false;
											continue;
										}
										promotion(b1,turn);
										movePiece(b1,turn);
										b1.printBoard();
										turn = true;
									}
								}/**End of Enp Passant Check **/
								else{ //else if piece is not a pawn do:
									if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("King")){
										oldKingX = startCoordinate.x;
										oldKingY = startCoordinate.y;
										//blacks turn thus:
										blackPieces.get(4).row = endCoordinate.x;
										blackPieces.get(4).col = endCoordinate.y;
										updateKingMoves(b1);
										if(check(turn, b1) == true){
											System.out.println("Illegal move, try again");
											checkMateBoolean = false;
									//if check is true, this will undo everything the moving of the king and say illegal:
											blackPieces.get(4).row = oldKingX;
											blackPieces.get(4).col = oldKingY;
											updateKingMoves(b1);
											continue;
										}
										
									}
									//checkMoveAfter = true;
									Player temp1 = new Player("brown");
									Pawn temp = new Pawn(temp1);
									if(b1.board[endCoordinate.x][endCoordinate.y].piece ==null) {
										//put temp pawn here
										//temp.player.color = "brown";
										b1.board[endCoordinate.x][endCoordinate.y].piece = temp;
									}
									updateKingMoves(b1);
									if(check(turn, b1) == true){
										System.out.println("Illegal move, try again");
										checkMateBoolean = false;
										continue;
									}
									promotion(b1,turn);
									movePiece(b1,turn);
									b1.printBoard();
									turn = true;
								}
							
							}
							else{
								System.out.println("Illegal move, try again");
								continue;
							}
							
						}
					}
				}
			}
		}

	}
	
	/**
	 * Translates input into valid 2d-array input **/
	public static boolean translateInput(String input, cord startCoordinate, cord endCoordinate, int dCounter,Board b1 ){
		//trim string in case of trailing spaces
		input = input.trim();
		
		/**
		 * xCord 1 and 2 are the letters from the x-axis
		 * which we will extract from the input and
		 * convert it to its respective int index.
		 */
		//char xCord1;
		//char xCord2;
		/**
		 * Similar to xCord, yCords are the ints extracted
		 * from input and converted to respective int indexes.
		 */
		//int yCord1;
		//int yCord2;
		
		String start = "";
		String end = "";
		//To read promotion of pawn OR "draw?"
		String promotion = "";
		// used to keep track of the index where each cord ends.
		int marker = 0;
		
		/**
		 * Gets the substrings for start and end coordinates,
		 * and for promotion (if any).
		 */
		for(int i = 0;i < input.length();i++){
			//gets the first substring of coords
			if(input.charAt(i) != ' ' && marker == 0){
				start += input.charAt(i);
			}
			//increase marker to ensure start is not overwritten; marker at 0 = start coord
			if(input.charAt(i) == ' ' && marker == 0){
				marker++; //increase marker from 0 to 1 to indicate that we are working on the end coord
				continue;
			}
			
			//gets the second substring of coords
			if(input.charAt(i) != ' ' && marker == 1){
				end += input.charAt(i);
			}
			
			//increase marker to ensure end is not overwritten; marker at 1 = end coord
			if(input.charAt(i) == ' ' && marker == 1){
				marker++;// change marker from 1 to 2 to indicate that we are working on draw/promotion
				continue;
			}
			
			//gets the promotion char from the input
			if(input.charAt(i) != ' ' && marker == 2){
				/**
				 * can either be a char indicating piece
				 * to be promoted to, or draw?/draw.
				 */
				promotion = input.substring(i);
				break;
			}
		}//end of for loop
		
		
		
		/** Converts user input coordinates to actual 2d array locations 
		 * and puts them into the global variable 'startCoordinate' & 'endCoordinate'
		 */
		//start of initializing startCoordinate.
		switch(start.charAt(0)){
		case 'a' : startCoordinate.y = 0;
		break;
		case 'b' : startCoordinate.y = 1;
		break;
		case 'c' : startCoordinate.y = 2;
		break;
		case 'd' : startCoordinate.y = 3;
		break;
		case 'e' : startCoordinate.y = 4;
		break;
		case 'f' : startCoordinate.y = 5;
		break;
		case 'g' : startCoordinate.y = 6;
		break;
		case 'h' : startCoordinate.y = 7;
		break; 
//ensures that if any other letter besides a thru h is entered then it is automatically an invalid move
		default : return false; 
		
		}
		
		switch(start.charAt(1)){
		case '8' : startCoordinate.x = 0;
		break;
		case '7' : startCoordinate.x = 1;
		break;
		case '6' : startCoordinate.x = 2;
		break;
		case '5' : startCoordinate.x = 3;
		break;
		case '4' : startCoordinate.x = 4;
		break;
		case '3' : startCoordinate.x = 5;
		break;
		case '2' : startCoordinate.x = 6;
		break;
		case '1' : startCoordinate.x = 7;
		break;
//ensures that if any other number besides 1 thru 8 is entered then it is automatically an invalid move
		default : return false;
		}/*** End of initializing startCoordinate.***/
		
		//start of initializing endCoordinate.
		switch(end.charAt(0)){
		case 'a' : endCoordinate.y = 0;
		break;
		case 'b' : endCoordinate.y = 1;
		break;
		case 'c' : endCoordinate.y = 2;
		break;
		case 'd' : endCoordinate.y = 3;
		break;
		case 'e' : endCoordinate.y = 4;
		break;
		case 'f' : endCoordinate.y = 5;
		break;
		case 'g' : endCoordinate.y = 6;
		break;
		case 'h' : endCoordinate.y = 7;
		break;
//ensures that if any other letter besides a thru h is entered then it is automatically an invalid move
		default : return false;
		}
		
		switch(end.charAt(1)){
		case '8' : endCoordinate.x = 0;
		break;
		case '7' : endCoordinate.x = 1;
		break;
		case '6' : endCoordinate.x = 2;
		break;
		case '5' : endCoordinate.x = 3;
		break;
		case '4' : endCoordinate.x = 4;
		break;
		case '3' : endCoordinate.x = 5;
		break;
		case '2' : endCoordinate.x = 6;
		break;
		case '1' : endCoordinate.x = 7;
		break;
//ensures that if any other number besides 1 thru 8 is entered then it is automatically an invalid move
		default : return false;
		}/*** end of initializing endCoordinate ***/
				
		/**
		 * After initializing the start and end coordinates we need to check if a draw is proposed
		 */
				if (promotion.equals("draw?")){
// resets draw counter to ensure that if a draw was proposed earlier in the game AND not taken, then this reset will handle it
					drawCounter = 0; 
					drawCounter++;
					return true;
				}
				
		promotionG = "";
		promotionG += promotion;
		return true;
	}
	
	
	public static void movePiece(Board b1, boolean turn){
		if (enPassant == null){
			//gets piece occupying the start tile
			Piece oldPiece = b1.board[startCoordinate.x][startCoordinate.y].piece;
			
			//changes the start tile's piece to null
			b1.board[startCoordinate.x][startCoordinate.y].piece = null;
			if (turn == true){
				try{
					if(blackPieces.contains(b1.board[endCoordinate.x][endCoordinate.y].piece)){
						int index = b1.board[endCoordinate.x][endCoordinate.y].piece.index;
						blackPieces.set(index, null);
					}
					
				}
				catch(NullPointerException ex){
				}
			}
			else{
				try{
					if(whitePieces.contains(b1.board[endCoordinate.x][endCoordinate.y].piece)){
						int index = b1.board[endCoordinate.x][endCoordinate.y].piece.index;
						whitePieces.set(index, null);
					}
				}
				catch(NullPointerException ex){
				}
			}
			//change the destination tile's piece
			b1.board[endCoordinate.x][endCoordinate.y].piece = oldPiece;
			
			oldPiece.row = endCoordinate.x;
			oldPiece.col = endCoordinate.y;
			
			//update last move's coordinates mainly for En Passant
			lastX = endCoordinate.x;
			lastY = endCoordinate.y;
		}
		else if(enPassant != null){
			if (enPassant.equals("white")){
				//gets piece occupying the start tile
				Piece oldPiece = b1.board[startCoordinate.x][startCoordinate.y].piece;
				b1.board[startCoordinate.x][startCoordinate.y].piece.row = endCoordinate.x;
				b1.board[startCoordinate.x][startCoordinate.y].piece.col = endCoordinate.y;
				//changes the start tile's piece to null
				b1.board[startCoordinate.x][startCoordinate.y].piece = null;
				
				//change the destination tile's piece
				b1.board[endCoordinate.x][endCoordinate.y].piece = oldPiece;
				//change the tile below the destination file to get rid of ('capture') the occupying piece
				b1.board[endCoordinate.x+1][endCoordinate.y].piece = null;
				//update last move's coordinates mainly for En Passant
				lastX = endCoordinate.x;
				lastY = endCoordinate.y;
			}
			else { //if the player is black:
				//gets piece occupying the start tile
				Piece oldPiece = b1.board[startCoordinate.x][startCoordinate.y].piece;
				b1.board[startCoordinate.x][startCoordinate.y].piece.row = endCoordinate.x;
				b1.board[startCoordinate.x][startCoordinate.y].piece.col = endCoordinate.y;
				//changes the start tile's piece to null
				b1.board[startCoordinate.x][startCoordinate.y].piece = null;
				
				//change the destination tile's piece
				b1.board[endCoordinate.x][endCoordinate.y].piece = oldPiece;
				//change the tile below the destination file to get rid of ('capture') the occupying piece
				b1.board[endCoordinate.x-1][endCoordinate.y].piece = null;
				//update last move's coordinates mainly for En Passant
				lastX = endCoordinate.x;
				lastY = endCoordinate.y;
				
			}
		}
	}
	
	public static void populateList(Board b1){
		for(int i = 0; i < 2; i++){
			for(int k = 0; k < 8; k++){
				blackPieces.add(b1.board[i][k].piece);
			}
		}
	//we have to this backwards from the black pieces list so that they are in the same format: 1-7 = named pieces 8-15 = pawn
		for (int i = 7; i > 5; i--){
			for (int k = 0; k < 8; k++){
				whitePieces.add(b1.board[i][k].piece);
			}
		}
		/**After populating the list we need to give each piece an index so we can reference them later with ease:
		 * Indexes: Rook = 0 & 7; Knight = 1 & 6; Bishop = 2 & 5;
		 *  Queen = 3; King = 4; 
		 *  Pawn = 8 thru 15
		 */
		for(int i = 0; i < whitePieces.size(); i++){ 
			whitePieces.get(i).index = i;
			//might as well update black pieces' indices as well
			blackPieces.get(i).index = i;
		}
		
	}
	
	/** THE FOLLOWING 'updateKingMoves' method does:
	 *  check all possible king moves:
	 *  1) make sure the moves are in bounds 
	 *	2) make sure there is NO FRIENDLY piece occupying the piece
	 *	3) If there is an opponent piece occupying the tile then this is still a valid move OR if tile is empty	*/
	
	public static void updateKingMoves(Board b1){
		//get and create current king coords, wks = white king start, bks = black king start:
		cord wks = new cord(whitePieces.get(4).row,whitePieces.get(4).col); //for white then add to king moves list at 0
		cord bks = new cord(blackPieces.get(4).row,blackPieces.get(4).col); //for black then add to king moves list at 0
		
		//after doing this we have to update the king move list to so that it removes the other presence of the new starting point
		try{
			if(whiteKingMoves.get(0).x != wks.x || whiteKingMoves.get(0).y != wks.y){
				//if the king moves then we set the list to null and add wks at 0 index
				whiteKingMoves.clear();
				whiteKingMoves.add(wks);
				
			}
			if(blackKingMoves.get(0).x != bks.x || blackKingMoves.get(0).y != bks.y){
				blackKingMoves.clear();
				blackKingMoves.add(bks);
			}
			
		}
		catch(IndexOutOfBoundsException ex){
			whiteKingMoves.add(0, wks);
			blackKingMoves.add(0, bks);
		}
		
			
		
		////////////////////////////////////white////////////////////////
		/**the following 2 variables can be seen as the 1st (top left corner) entry in a 3x3 matrix
		 * where the center is the current king position*/
		int whiteStartRow = wks.x - 1;
		int whiteStartCol = wks.y - 1;
		/**Traverse the 3x3 matrix of possible moves and distinguish which moves are in and out of bounds starting from
		 * the top left of the matrix: */
		for(int row = whiteStartRow; row < whiteStartRow + 3; row++){
			if(row < 0 || row > 7 ) continue;
			for(int col = whiteStartCol; col < whiteStartCol + 3; col ++){
				if(col < 0 || col > 7 ) continue;
				if (row == whiteKingMoves.get(0).x && col == whiteKingMoves.get(0).y) continue;
				/** If it has reached this point then all of the moves are in bound
				 * so now we check if there are friendly pieces occupying these tiles:*/
				if(b1.board[row][col].piece == null){ //no occupying piece on the tile thus automatically a valid move
					cord temp = new cord(row, col);
					if(kmHelper(temp.x,temp.y,whiteKingMoves) != true) whiteKingMoves.add(temp);
					;
				}
				else if (!b1.board[row][col].piece.player.color.equals("white")){//capturing a piece is a valid move
					cord temp = new cord(row, col);
					if(kmHelper(temp.x,temp.y,whiteKingMoves) != true) whiteKingMoves.add(temp);
				}
				/**no need for an if statement where they are the same color because if this occurs then it is an invalid
				 * move and doesn't get added to the list of current valid moves aka whiteKingMoves*/
				
			}
		}
		////////////////////////////////////white////////////////////////
		////////////////////////////////////black////////////////////////
		//SAME CODE AS ABOVE EXCEPT MODIFIED FOR BLACK//
		/**the following 2 variables can be seen as the 1st (top left corner) entry in a 3x3 matrix
		 * where the center is the current king position*/
		int blackStartRow = bks.x - 1;
		int blackStartCol = bks.y - 1;
		/**Traverse the 3x3 matrix of possible moves and distinguish which moves are in and out of bounds starting from
		 * the top left of the matrix: */
		for(int row = blackStartRow; row < blackStartRow + 3; row++){
			if(row < 0 || row > 7 ) continue;
			for(int col = blackStartCol; col < blackStartCol + 3; col ++){
				if(col < 0 || col > 7 ) continue;
				if (row == blackKingMoves.get(0).x && col == blackKingMoves.get(0).y) continue;
				/** If it has reached this point then all of the moves are in bound
				 * so now we check if there are friendly pieces occupying these tiles:*/
				if(b1.board[row][col].piece == null){ //no occupying piece on the tile thus automatically a valid move
					cord temp = new cord(row, col);
					if(kmHelper(temp.x,temp.y,blackKingMoves) != true) blackKingMoves.add(temp);
				}
				else if (!b1.board[row][col].piece.player.color.equals("black")){//capturing a piece is a valid move
					cord temp = new cord(row, col);
					if(kmHelper(temp.x,temp.y,blackKingMoves) != true) blackKingMoves.add(temp);				}
				/**no need for an if statement where they are the same color because if this occurs then it is an invalid
				 * move and doesn't get added to the list of current valid moves aka whiteKingMoves*/
			}
		}
		
	}
	/**Used to help switch places of coordinates in kingMoves lists for when the king moves*/
	public static boolean kmHelper(int x, int y, ArrayList<cord> kingMoves){
		for(int i = 0; i < kingMoves.size(); i ++){
			if( x == kingMoves.get(i).x && y == kingMoves.get(i).y){
				return true; //It is true that there is a duplicate
			}
		}
		return false;
	}

	public static boolean check(boolean turn, Board b1){
		/**turn = true means it's white's turn and we should use blackPieces list checked against whiteKingMove list*/
		if (turn == true){
			for(int i = 0; i < blackPieces.size();i++){
				if(blackPieces.get(i) == null) continue;
				cord start = new cord(blackPieces.get(i).row, blackPieces.get(i).col);
				cord end = new cord(whiteKingMoves.get(0).x, whiteKingMoves.get(0).y);
				
				if(blackPieces.get(i).validMovement2(start, end, b1) == true){
					checkMate(b1, turn);
					return true;
				}
				
			}
			
		}
		else{ //if turn = false means that it's black's turn
			for(int i = 0; i < whitePieces.size();i++){
				if(whitePieces.get(i) == null) continue;
				cord start = new cord(whitePieces.get(i).row, whitePieces.get(i).col);
				cord end = new cord(blackKingMoves.get(0).x, blackKingMoves.get(0).y);

				if(whitePieces.get(i).validMovement2(start, end, b1) == true){
					
					checkMate(b1, turn);
					return true;
				}
				
			}
			
		}
		//returns false if there are no moves that put the king in check
		return false;
	}
	
	public static void checkMate(Board b1, boolean turn ){
		ArrayList<cord> allPossibleMoves = new ArrayList<cord>();
		if(turn == true){ //if white's turn
			/** if it is white's turn then we check each piece's validMovement function in blackPieces
			 * against all of the coordinates inside of whiteKingMoves. If we have a match we:
			 * 1)check for duplicates to make sure that this end coordinate (from whiteKingMoves) isn't 
			 * already accounted for/included inside of allPossibleMoves.
			 * 2) if the end coordinate is not accounted for then we add it to the allPossibleMoves list. 
			 */
			for(int i = 0; i < blackPieces.size();i++ ){
				if(blackPieces.get(i) == null) continue;
				for(int k=0; k < whiteKingMoves.size(); k++){
					if(whiteKingMoves.get(k) == null) continue;
					cord start = new cord(blackPieces.get(i).row, blackPieces.get(i).col);
					cord end = new cord(whiteKingMoves.get(k).x,whiteKingMoves.get(k).y);
					if(blackPieces.get(i).validMovement2(start, end, b1) == true){
						debug(5);
						try{
							if(allPossibleMoves.size() == 0){
								allPossibleMoves.add(end);
							}
							//duplicate check
							else{
								for(int z = 0; z < allPossibleMoves.size(); z ++){
									if(allPossibleMoves.get(z) == null) continue;
									if(end.x != allPossibleMoves.get(z).x && end.y != allPossibleMoves.get(z).y){
										allPossibleMoves.add(end); //It is true that there is a duplicate
									}
								}
							}
						}
						catch(IndexOutOfBoundsException ex){
							allPossibleMoves.add(end);
						}
					}
				}
			}
			//clone of whiteKingMoves
			ArrayList <cord> clone = new ArrayList<cord>(whiteKingMoves.size());
			for(int i = 0; i< whiteKingMoves.size(); i++){
				clone.add(whiteKingMoves.get(i));
			}
			clone.removeAll(Collections.singleton(null));
			allPossibleMoves.removeAll(Collections.singleton(null));
			if(clone.size() == allPossibleMoves.size()){
				checkMateBoolean = true;
			}
		}
		else{ //if it's black's turn
			for(int i = 0; i < whitePieces.size();i++ ){
				if(whitePieces.get(i) == null) continue;
				for(int k=0; k < blackKingMoves.size(); k++){
					if(blackKingMoves.get(k) == null) continue;
					cord start = new cord(whitePieces.get(i).row, whitePieces.get(i).col);
					cord end = new cord(blackKingMoves.get(k).x,blackKingMoves.get(k).y);
					if(whitePieces.get(i).validMovement2(start, end, b1) == true){
						debug(5);
						try{
							if(allPossibleMoves.size() == 0){ //change here to check if size == 0 then add
								allPossibleMoves.add(end);
							}
							//duplicate check
							else{
								for(int z = 0; z < allPossibleMoves.size(); z ++){
									if(allPossibleMoves.get(z) == null) continue;
									if(end.x != allPossibleMoves.get(z).x && end.y != allPossibleMoves.get(z).y){
										allPossibleMoves.add(end); //It is true that there is a duplicate
									}
								}
							}
						}
						catch(IndexOutOfBoundsException ex){
							allPossibleMoves.add(end);
						}
					}
				}
			}
			//clone of blackKingMoves
			ArrayList <cord> clone = new ArrayList<cord>(blackKingMoves.size());
			for(int i = 0; i< blackKingMoves.size(); i++){
				clone.add(blackKingMoves.get(i));
			}
			clone.removeAll(Collections.singleton(null));
			allPossibleMoves.removeAll(Collections.singleton(null));
			if(clone.size()-1 == allPossibleMoves.size()){
				checkMateBoolean = true;
			}
			
		}
		
	}
	//used for promotion
	static void promotion(Board b1,boolean turn){
		if(promotionG.equals("")) promotionG += "q";
		if(turn == true){//if it is white's turn
			//check if the piece at startCoord is a pawn if it isn't then do nothing and set promotionG to null(at end)
			if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("Pawn")){
				if(endCoordinate.x == 0){//if it reaches here then we know it can be promoted
					promotionG.trim();
					promotionG.toLowerCase();
					//get all of the current pawn's properties into a variable:
					int col = b1.board[startCoordinate.x][startCoordinate.y].piece.col;
					int row = b1.board[startCoordinate.x][startCoordinate.y].piece.row;
					int index = b1.board[startCoordinate.x][startCoordinate.y].piece.index;
					int moveCount = b1.board[startCoordinate.x][startCoordinate.y].piece.moveCount;
					Player player = b1.board[startCoordinate.x][startCoordinate.y].piece.player;
					
					switch(promotionG.charAt(0)){
						case 'r':
							Rook rook = new Rook(row,col,index,player,moveCount);
							rook.pieceAbriv = "wR";
							whitePieces.set(index, rook);
							b1.board[startCoordinate.x][startCoordinate.y].piece = rook;
							break;
						case 'n':
							Knight knight = new Knight(row,col,index,player,moveCount);
							knight.pieceAbriv = "wK";
							whitePieces.set(index, knight);
							b1.board[startCoordinate.x][startCoordinate.y].piece = knight;
							break;
						case 'b':
							Bishop bishop = new Bishop(row,col,index,player,moveCount);
							bishop.pieceAbriv = "wB";
							whitePieces.set(index, bishop);
							b1.board[startCoordinate.x][startCoordinate.y].piece = bishop;
							break;
						case 'q':
							Queen queen = new Queen(row,col,index,player,moveCount);
							queen.pieceAbriv = "wQ";
							whitePieces.set(index, queen);
							b1.board[startCoordinate.x][startCoordinate.y].piece = queen;
							break;
						case 'k':
							Knight knight1 = new Knight(row,col,index,player,moveCount);
							knight1.pieceAbriv = "wK";
							whitePieces.set(index, knight1);
							b1.board[startCoordinate.x][startCoordinate.y].piece = knight1;
							break;
							
						default:
							Queen queen1 = new Queen(row,col,index,player,moveCount);
							queen1.pieceAbriv = "wQ";
							whitePieces.set(index, queen1);
							b1.board[startCoordinate.x][startCoordinate.y].piece = queen1;//make queen 
					}
					
						
					
				}
			}
		}
		//for black promotion
		else if(turn == false){
			//check if the piece at startCoord is a pawn if it isn't then do nothing and set promotionG to null(at end)
			if(b1.board[startCoordinate.x][startCoordinate.y].piece.getPiece().equals("Pawn")){
				if(endCoordinate.x == 7){//if it reaches here then we know it can be promoted
					promotionG.trim();
					promotionG.toLowerCase();
					//get all of the current pawn's properties into a variable:
					int col = b1.board[startCoordinate.x][startCoordinate.y].piece.col;
					int row = b1.board[startCoordinate.x][startCoordinate.y].piece.row;
					int index = b1.board[startCoordinate.x][startCoordinate.y].piece.index;
					int moveCount = b1.board[startCoordinate.x][startCoordinate.y].piece.moveCount;
					Player player = b1.board[startCoordinate.x][startCoordinate.y].piece.player;
					
					
					switch(promotionG.charAt(0)){
						case 'r':
							Rook rook = new Rook(row,col,index,player,moveCount);
							rook.pieceAbriv = "bR";
							blackPieces.set(index, rook);
							b1.board[startCoordinate.x][startCoordinate.y].piece = rook;
							break;
						case 'n':
							Knight knight = new Knight(row,col,index,player,moveCount);
							knight.pieceAbriv = "bK";
							blackPieces.set(index, knight);
							b1.board[startCoordinate.x][startCoordinate.y].piece = knight;
							break;
						case 'b':
							Bishop bishop = new Bishop(row,col,index,player,moveCount);
							bishop.pieceAbriv = "bB";
							blackPieces.set(index, bishop);
							b1.board[startCoordinate.x][startCoordinate.y].piece = bishop;
							break;
						case 'q':
							Queen queen = new Queen(row,col,index,player,moveCount);
							queen.pieceAbriv = "bQ";
							blackPieces.set(index, queen);
							b1.board[startCoordinate.x][startCoordinate.y].piece = queen;
							break;
						case 'k':
							Knight knight1 = new Knight(row,col,index,player,moveCount);
							knight1.pieceAbriv = "bK";
							blackPieces.set(index, knight1);
							b1.board[startCoordinate.x][startCoordinate.y].piece = knight1;
							break;
							
						default:
							Queen queen1 = new Queen(row,col,index,player,moveCount);
							queen1.pieceAbriv = "bQ";
							blackPieces.set(index, queen1);
							b1.board[startCoordinate.x][startCoordinate.y].piece = queen1;//make queen 
					}
					
						
					
				}
			}
		}
		//resets promotion
		promotionG = "";
	}
		public static void printToFile(Board b1, boolean turn) {
		String file = "games.txt";
		BufferedWriter bw = null;
		
		try{
			bw = new BufferedWriter(new FileWriter(file, true));
			//get start coordinates
			int startX = startCoordinate.x;
			int startY = startCoordinate.y;
			
			//end coordinates of piece being moved
			int endX = b1.board[endCoordinate.x][endCoordinate.y].piece.row;
			int endY = b1.board[endCoordinate.x][endCoordinate.y].piece.col;
			
			//color and piece String to be saved on each turn
			String piece = b1.board[endCoordinate.x][endCoordinate.y].piece.getPiece();
			String color;
			if (turn) color = "white";
			else color = "black";
			 
			bw.append(color + ", " + piece + ", " + startX + ", " + startY + ", " + endX + ", " + endY + "\r\n" );
			System.out.println(color + "" + piece + "" + startX + "" + startY + "" + endX + "" + endY);
			
			bw.close();
						
			
		}
		catch(IOException e) {
			System.out.println("Save file not working");
		}
		Platform.exit();
		
	}
	
	public static void readFromFile() {
		
		File file = new File("games.txt");
		String[] words = null;
		String [][] game = null;
		
		try{
			
			BufferedReader  br = new BufferedReader(new FileReader(file));
			String input;
		
			while((input = br.readLine()) != null) {
				words = input.split(",");
				for(int i = 0; i < words.length; i++) {
					
					System.out.println(words[i]);
				}
			}
			for(int i = 0; i < words.length; i++) {
				
				System.out.println(words[i]);
			}
			br.close();
		}
		catch(IOException e) {
			System.out.println("Reader not working");
		}
		
	}
	
	static void debug(int i ){
		i++;
	}
}

