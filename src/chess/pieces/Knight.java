package chess.pieces;
import chess.Board;
import chess.Piece;
import chess.Player;
import chess.cord;
/**
 * @author andrewgonzalez
 * @author danielrodriguez
 */
public class Knight extends Piece {
	
	
	public Knight(){
	}
	
	public Knight(Player color){
		this.player = color;
	}
	public Knight(int row, int col, int index, Player color, int moveCount){
		this.row = row;
		this.col = col;
		this.index = index;
		this.player = color;
		this.moveCount = moveCount;
	}
	

	@Override
	public void Piece(Player color, chess.Piece type) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean validMovement(cord start, cord end, Board b1) {
		int startX = start.x;
		int startY = start.y;
		int targetX = end.x;
		int targetY = end.y;
		
		int diffInX = targetX-startX;
		int diffInY = targetY-startY;
		int absoluteDiff = Math.abs(diffInX) + Math.abs(diffInY);
		//makes sure the knight is not moving straight in any direction
		if(diffInX == 0 || diffInY == 0){
			return false;
		}
		/*
		 * check to see if piece is moving correctly in the L shape:
		 * because the difference from start[n][k] to end[n][k]
		 * should have an absolute value of 3 from all possible moves.
		 */
		if(absoluteDiff != 3){
			return false;
		}
		else{
			//this.row = targetX;
			//this.col = targetY;
			moveCount++;
			return true;
		}
	}
	
	@Override
	public boolean validMovement2(cord start, cord end, Board b1) {
		int startX = start.x;
		int startY = start.y;
		int targetX = end.x;
		int targetY = end.y;
		
		int diffInX = targetX-startX;
		int diffInY = targetY-startY;
		int absoluteDiff = Math.abs(diffInX) + Math.abs(diffInY);
		//makes sure the knight is not moving straight in any direction
		if(diffInX == 0 || diffInY == 0){
			return false;
		}
		/*
		 * check to see if piece is moving correctly in the L shape:
		 * because the difference from start[n][k] to end[n][k]
		 * should have an absolute value of 3 from all possible moves.
		 */
		if(absoluteDiff != 3){
			return false;
		}
		else{
			//this.row = targetX;
			//this.col = targetY;
			return true;
		}
	}
	
	

	@Override
	public String getPiece() {
		// TODO Auto-generated method stub
		return "Knight";
	}
	
	

	
}
