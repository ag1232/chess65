package chess.pieces;
import chess.Board;
import chess.Piece;
import chess.Player;
import chess.cord;
/**
 * @author andrewgonzalez
 * @author danielrodriguez
 */
public class Rook extends Piece {
	
	
	public Rook(){
	}
	
	public Rook(Player color){
		this.player = color;
	}
	
	

	@Override
	public void Piece(Player color, chess.Piece type) {
		// TODO Auto-generated method stub
		
	}
	public Rook(int row, int col, int index, Player color, int moveCount){
		this.row = row;
		this.col = col;
		this.index = index;
		this.player = color;
		this.moveCount = moveCount;
	}


	@Override
	public String getPiece() {
		// TODO Auto-generated method stub
		return "Rook" ;
	}

	@Override
	public boolean validMovement(cord start, cord end, Board b1) {
		int startX = start.x;
		int startY = start.y;
		int targetX = end.x;
		int targetY = end.y;
		
		int diffInX = targetX-startX;
		int diffInY = targetY-startY;
		int counter;
		//int absoluteDiff = Math.abs(diffInX) + Math.abs(diffInY);
		if(diffInX != 0 && diffInY != 0){
			return false;
		}
		if(diffInX == 0 && diffInY ==0){
			return false;
		}
		
		//if it moves vertically
		if(diffInX != 0){
			counter = Math.abs(diffInX);
			if(diffInX < 0){// Rook Moves UP Vertically
				for(int i = 0; i<  counter; i++){
					startX--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					//check if piece on tile is of the same color/ally
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							
							else {
								//this.row = targetX;
								//this.col = targetY;
								moveCount++;
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
						
					}
//					/**
//					 * if the tile is NOT a)Empty and b)of the same color/team
//					 * then return valid move (the capturing is done in the
//					 * movePiece method of the Main.java file.
//					 */
//					else if(){
//						
//						this.row = targetX;
//						this.col = targetY;
//						
//						return true;
//					}
				}
			}
			else{// Rook moves DOWN Vertically
				for(int i = 0; i<  counter; i++){
					startX++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							else {
//								this.row = targetX;
//								this.col = targetY;
								moveCount++;
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
//					/**
//					 * if the tile is NOT a)Empty and b)of the same color/team
//					 * then return valid move (the capturing is done in the
//					 * movePiece method of the Main.java file.
//					 */
//					else{
//						this.row = targetX;
//						this.col = targetY;
//						return true;
//					}
				}
			}
			
		}
		else{//Moves Horizontally
			counter = Math.abs(diffInY);
			if(diffInY < 0){// Rook Moves LEFT horizontally
				for(int i = 0; i<  counter; i++){
					startY--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							else {
//								this.row = targetX;
//								this.col = targetY;
								moveCount++;
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			else{// Rook moves RIGHT horizontally
				for(int i = 0; i<  counter; i++){
					startY++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							else{
//								this.row = targetX;
//								this.col = targetY;
								moveCount++;
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
		}
		
		// TODO Auto-generated method stub
//		this.row = targetX;
//		this.col = targetY;
		return true;
	}
	
	@Override
	public boolean validMovement2(cord start, cord end, Board b1){
		int startX = start.x;
		int startY = start.y;
		int targetX = end.x;
		int targetY = end.y;
		
		int diffInX = targetX-startX;
		int diffInY = targetY-startY;
		int counter;
		//int absoluteDiff = Math.abs(diffInX) + Math.abs(diffInY);
		if(diffInX != 0 && diffInY != 0){
			return false;
		}
		if(diffInX == 0 && diffInY ==0){
			return false;
		}
		
		//if it moves vertically
		if(diffInX != 0){
			counter = Math.abs(diffInX);
			if(diffInX < 0){// Rook Moves UP Vertically
				for(int i = 0; i<=  counter; i++){
					startX--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							if(i == counter){//last move
								if(startX == targetX && startY == targetY){
									return true;
								}else return false;							
							}
							else return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			else{// Rook moves DOWN Vertically
				for(int i = 0; i<=  counter; i++){
					startX++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							if(i == counter){//last move
								if(startX == targetX && startY == targetY){
									return true;
								}else return false;							
							}
							else return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			
		}
		else{//Moves Horizontally
			counter = Math.abs(diffInY);
			if(diffInY < 0){// Rook Moves LEFT horizontally
				for(int i = 0; i<=  counter; i++){
					startY--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							if(i == counter){//last move
								if(startX == targetX && startY == targetY){
									return true;
								}else return false;							
							}
							else return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			else{// Rook moves RIGHT horizontally
				for(int i = 0; i<=  counter; i++){
					startY++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							if(i == counter){//last move
								if(startX == targetX && startY == targetY){
									return true;
								}else return false;							
							}
							else return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
		}
		
		// TODO Auto-generated method stub
		return false;
	} 


}
