package chess.pieces;
import chess.Board;
import chess.Piece;
import chess.Player;
import chess.cord;
/**
 * @author andrewgonzalez
 * @author danielrodriguez
 */
public class Queen extends Piece{
	
	
	public Queen(){
	}
	
	public Queen(Player color){
		this.player = color;
	}
	
	

	@Override
	public void Piece(Player color, chess.Piece type) {
		// TODO Auto-generated method stub
		
	}
	public Queen(int row, int col, int index, Player color, int moveCount){
		this.row = row;
		this.col = col;
		this.index = index;
		this.player = color;
		this.moveCount = moveCount;
	}


	@Override
	public boolean validMovement(cord start, cord end, Board b1) {
		if(rookMovement(start, end, b1) == true || bishopMovement(start, end, b1) == true ){
			//this.row = end.x;
			//this.col = end.y;
			moveCount++;
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public String getPiece() {
		// TODO Auto-generated method stub
		return "Queen";
	}
	
	
	//copied and pasted from Rook class
	public boolean rookMovement(cord start, cord end, Board b1){
		int startX = start.x;
		int startY = start.y;
		int targetX = end.x;
		int targetY = end.y;
		
		int diffInX = targetX-startX;
		int diffInY = targetY-startY;
		int counter;
		//int absoluteDiff = Math.abs(diffInX) + Math.abs(diffInY);
		if(diffInX != 0 && diffInY != 0){
			return false;
		}
		if(diffInX == 0 && diffInY ==0){
			return false;
		}
		
		//if it moves vertically
		if(diffInX != 0){
			counter = Math.abs(diffInX);
			if(diffInX < 0){// Rook Moves UP Vertically
				for(int i = 0; i<  counter; i++){
					startX--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							else{
								//this.row = targetX;
								//this.col = targetY;
								
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			else{// Rook moves DOWN Vertically
				for(int i = 0; i<  counter; i++){
					startX++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							else{
								//this.row = targetX;
								//this.col = targetY;
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			
		}
		else{//Moves Horizontally
			counter = Math.abs(diffInY);
			if(diffInY < 0){// Rook Moves LEFT horizontally
				for(int i = 0; i<  counter; i++){
					startY--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							else{
								//this.row = targetX;
								//this.col = targetY;
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			else{// Rook moves RIGHT horizontally
				for(int i = 0; i<  counter; i++){
					startY++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							else{
								//this.row = targetX;
								//this.col = targetY;
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
		}
		
		// TODO Auto-generated method stub
		return true;
	} 
	
	
	
	//copied and pasted from Bishop class
	public boolean bishopMovement(cord start, cord end, Board b1){
		int startX = start.x;
		int startY = start.y;
		int targetX = end.x;
		int targetY = end.y;
		
		int diffInX = targetX - startX;
		int diffInY = targetY - startY;
		//check to see if the bishop is moving diagonally 
		if(diffInX == 0){
			return false;
		}
		if (Math.abs((diffInY/diffInX)) != 1){
			return false;
		}
		
		/**
		 * the abs value of diffX&Y should be equal, therefore
		 * using either is valid to use as a counter for how 
		 * many tiles to check in the correct direction
		 * which is determined below.
		 */
		int counter = Math.abs(diffInX);
		
		
		//check vertical dirrection of piece
		if(diffInX < 0){ //if difference in x<0--> moves UP in direction
			//check horizontal direction of piece
			if(diffInY < 0){ //if diff in y<0--> moves LEFT in direction
				/**
				 * Implement for loop to check tiles up and to the LEFT
				 * in direction until the target tile is reached
				 */
				for(int i = 0; i<  counter; i++){
					startX--;
					startY--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							else{
								//this.row = targetX;
								//this.col = targetY;
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
				
			}
			else{// if diff in y>0--> moves RIGHT in direction
				/**
				 * Implement for loop to check tiles up and to the RIGHT
				 * in direction until the target tile is reached
				 */
				for(int i = 0; i<  counter; i++){
					startX--;
					startY++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							else{
								//this.row = targetX;
								//this.col = targetY;
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			
		}
		else{ //If diff in x>0--> the piece moves DOWN in direction
			//check horizontal direction of piece
			if(diffInY < 0){ //if diff in y<0--> moves LEFT in direction
				/**
				 * Implement for loop to check tiles up and to the LEFT
				 * in direction until the target tile is reached
				 */
				for(int i = 0; i<  counter; i++){
					startX++;
					startY--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							else{
								//this.row = targetX;
								//this.col = targetY;
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
				
			}
			else{// if diff in y>0--> moves RIGHT in direction
				/**
				 * Implement for loop to check tiles up and to the RIGHT
				 * in direction until the target tile is reached
				 */
				for(int i = 0; i<  counter; i++){
					startX++;
					startY++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						continue;
					}
					else if(b1.board[startX][startY].piece != null){
						if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)){
								return false;
							}
							else{
								//this.row = targetX;
								//this.col = targetY;
								return true;
							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			
		}
		//this.row = targetX;
		//this.col = targetY;
		return true;
		
	}
	/**********************************************************************************************/
	/**********************************************************************************************/
	/**********************************************************************************************/
	public boolean validMovement2(cord start, cord end, Board b1) {
		if(rookMovement2(start, end, b1) == true || bishopMovement2(start, end, b1) == true ){
			//this.row = end.x;
			//this.col = end.y;
			return true;
		}
		else{
			return false;
		}
	}
	
	
	//copied and pasted from Rook class
	public boolean rookMovement2(cord start, cord end, Board b1){
		int startX = start.x;
		int startY = start.y;
		int targetX = end.x;
		int targetY = end.y;
		
		int diffInX = targetX-startX;
		int diffInY = targetY-startY;
		int counter;
		//int absoluteDiff = Math.abs(diffInX) + Math.abs(diffInY);
		if(diffInX != 0 && diffInY != 0){
			return false;
		}
		if(diffInX == 0 && diffInY ==0){
			return false;
		}
		
		//if it moves vertically
		if(diffInX != 0){
			counter = Math.abs(diffInX);
			if(diffInX < 0){// Rook Moves UP Vertically
				for(int i = 0; i<=  counter; i++){
					startX--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							if(i == counter){//last move
								if(startX == targetX && startY == targetY){
									return true;
								}else return false;							
							}
							else return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			else{// Rook moves DOWN Vertically
				for(int i = 0; i<=  counter; i++){
					startX++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							if(i == counter){//last move
								if(startX == targetX && startY == targetY){
									return true;
								}else return false;							
							}
							else return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			
		}
		else{//Moves Horizontally
			counter = Math.abs(diffInY);
			if(diffInY < 0){// Rook Moves LEFT horizontally
				for(int i = 0; i<=  counter; i++){
					startY--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							if(i == counter){//last move
								if(startX == targetX && startY == targetY){
									return true;
								}else return false;							
							}
							else return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			else{// Rook moves RIGHT horizontally
				for(int i = 0; i<=  counter; i++){
					startY++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							if(i == counter){//last move
								if(startX == targetX && startY == targetY){
									return true;
								}else return false;							
							}
							else return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
		}
		
		// TODO Auto-generated method stub
		return false;
	} 
	
	
	
	//copied and pasted from Bishop class
	public boolean bishopMovement2(cord start, cord end, Board b1){
		int startX = start.x;
		int startY = start.y;
		int targetX = end.x;
		int targetY = end.y;
		
		int diffInX = targetX - startX;
		int diffInY = targetY - startY;
		//check to see if the bishop is moving diagonally 
		if(diffInX == 0){
			return false;
		}
		if (Math.abs((diffInY/diffInX)) != 1){
			return false;
		}
		
		/**
		 * the abs value of diffX&Y should be equal, therefore
		 * using either is valid to use as a counter for how 
		 * many tiles to check in the correct direction
		 * which is determined below.
		 */
		int counter = Math.abs(diffInX);
		
		
		//check vertical dirrection of piece
		if(diffInX < 0){ //if difference in x<0--> moves UP in direction
			//check horizontal direction of piece
			if(diffInY < 0){ //if diff in y<0--> moves LEFT in direction
				/**
				 * Implement for loop to check tiles up and to the LEFT
				 * in direction until the target tile is reached
				 */
				for(int i = 0; i<=  counter; i++){
					startX--;
					startY--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							if(i == counter){//last move
								if(startX == targetX && startY == targetY){
									return true;
								}else return false;							
							}
							else return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
				
			}
			else{// if diff in y>0--> moves RIGHT in direction
				/**
				 * Implement for loop to check tiles up and to the RIGHT
				 * in direction until the target tile is reached
				 */
				for(int i = 0; i<=  counter; i++){
					startX--;
					startY++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							if(i == counter){//last move
								if(startX == targetX && startY == targetY){
									return true;
								}else return false;							
							}
							else return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			
		}
		else{ //If diff in x>0--> the piece moves DOWN in direction
			//check horizontal direction of piece
			if(diffInY < 0){ //if diff in y<0--> moves LEFT in direction
				/**
				 * Implement for loop to check tiles up and to the LEFT
				 * in direction until the target tile is reached
				 */
				for(int i = 0; i<=  counter; i++){
					startX++;
					startY--;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							if(i == counter){//last move
								if(startX == targetX && startY == targetY){
									return true;
								}else return false;							
							}
							else return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
				
			}
			else{// if diff in y>0--> moves RIGHT in direction
				/**
				 * Implement for loop to check tiles up and to the RIGHT
				 * in direction until the target tile is reached
				 */
				for(int i = 0; i<  counter; i++){
					startX++;
					startY++;
					/**
					 * check if there are 
					 * 1) Pieces to capture on the way OR
					 * 2)Alliance pieces which are hindering movement
					 */
					//check first to see if tile is empty, if so continue.
					if(b1.board[startX][startY].piece == null){
						if(startX == targetX && startY == targetY){
							return true;
						}else continue;
					}
					else if(b1.board[startX][startY].piece != null){
						//following is only used for check/checkmate:
						//if the piece is a king and of the opponents color AKA not friendly then return valid move:
						if(b1.board[startX][startY].piece.getPiece().equals("King") && !b1.board[startX][startY].piece.player.color.equals(this.player.color)){
							return true;
						}
						else if (i == counter -1){//check if piece is friendly if it is then we return false
							if(b1.board[startX][startY].piece.player.color.equals(this.player.color)|| !b1.board[startX][startY].piece.player.color.equals(this.player.color) ){
								return false;
							}
//							else{
//								//this.row = targetX;
//								//this.col = targetY;
//								return true;
//							}
						}
						else{
							return false; // if there are any pieces in the way
						}
					}
				}
			}
			
		}
		//this.row = targetX;
		//this.col = targetY;
		return false;
		
	}

}
