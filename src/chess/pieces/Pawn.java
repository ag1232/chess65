package chess.pieces;
import chess.Board;
import chess.Piece;
import chess.Player;
import chess.cord;

/**
 * @author andrewgonzalez
 * @author danielrodriguez
 */
public class Pawn extends Piece {
	//used for en passant and double move as 1st move
	public int moveCount = 0;
	public boolean doubleMove = false;
	public boolean enPassantBoolean = false;
	
	public Pawn(){
	}
	
	public Pawn(Player color){
		this.player = color;
	}
	

	@Override
	public void Piece(Player color, chess.Piece type) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getPiece() {
		// TODO Auto-generated method stub
		return "Pawn";
	}

	@Override
	public boolean validMovement(cord start, cord end, Board b1) {
		int startX = start.x;
		int startY = start.y;
		int targetX = end.x;
		int targetY = end.y;
		
		int diffInX = targetX - startX;
		int diffInY = targetY - startY;
		int absDiffX = Math.abs(diffInX);
		int absDiffY = Math.abs(diffInY);
		
		//Checks to makes sure no double move is done after first turn:
				if(absDiffX > 1 && moveCount > 0){ 
					return false;
				}
		
		//Checks if Pawn is ATTACKING:
		if(absDiffY == 1 && absDiffX == 1){
			/**
			 * makes sure that ONLY attacking is taking place
			 * when going diagonally. otherwise if piece tries to go
			 * diagonal while not attacking, it will check for En passant. 
			 */
			if(b1.board[targetX][targetY].isEmpty() == true){
				//cannot just use 'return enPassant()' because we have to properly increment the moveCount
				boolean valid = enPassant(targetX,targetY,b1);
				if(valid == true){
					moveCount++;
					enPassantBoolean = true;
//					this.row = targetX;
//					this.col = targetY;
					return true;
				}else{ return false;}
			}
			//note we don't have to check for friendly fire. Check is done in main.
			else{
				moveCount++;
				enPassantBoolean = false;
//				this.row = targetX;
//				this.col = targetY;
				return true;
			}
		}
		
		//Checks to see if the move is a) a FIRST move and b) a double move:
		if (absDiffX == 2 && diffInY == 0 && moveCount ==0){//checking for a & b
			
				if(this.player.color.equals("white")){// if piece is White
					// returns false if there is a piece in front of it (jumping is not allowed).
					if(b1.board[startX-1][startY].piece != null){
						return false;
					}
					else{
						moveCount++;
						doubleMove = true;
						enPassantBoolean = false;
//						this.row = targetX;
//						this.col = targetY;
						return true;
					}
				}
				else{// if piece is black
					// returns false if there is a piece in front of it (jumping is not allowed).
					if(b1.board[startX+1][startY].piece != null){
						return false;
					}
					else{
						moveCount++;
						doubleMove = true;
						enPassantBoolean = false;
//						this.row = targetX;
//						this.col = targetY;
						return true;
					}
				}
		}

		/**
		 * The difference in X should be < 0 for WHITE pawns (moving UP)
		 * The difference in X should be >0 for BLACK pawns(moving down)
		 */
		
		// for white pieces
		if(this.player.color.equals("white") && diffInX == -1 && diffInY == 0){
			if(targetX > startX){//this ensures forward movements only
				return false;
			}
			// returns false if there is a piece in front of it (jumping is not allowed).
			if(b1.board[startX-1][startY].piece != null){
				return false;
			}
			else{
				moveCount++;
				enPassantBoolean = false;
//				this.row = targetX;
//				this.col = targetY;
				return true;
			}
			
		}
		
		// for black pieces
		else if (this.player.color.equals("black") && diffInX == 1 && diffInY == 0){
			
			if(targetX < startX){//this ensures forward movements only
				return false;
			}
			// returns false if there is a piece in front of it (jumping is not allowed).
			if(b1.board[startX+1][startY].piece != null){
				return false;
			}
			else{
				moveCount++;
				enPassantBoolean = false;
//				this.row = targetX;
//				this.col = targetY;
				return true;
			}
			
		}
		/**
		 * check move count... if it is = 0, then moving 2 spaces is 
		 * valid; otherwise move 1 space
		 */
		
		// TODO Auto-generated method stub
		
		return false;
	}
	
	public boolean enPassant(int targetX, int targetY, Board b1){
		//Check the color of the current piece AKA the piece that is moving.
		if (this.player.color.equals("white")){ 
			Piece temp = b1.board[targetX+1][targetY].piece;
			if (temp == null) return false;
			//returns false if the piece is not a pawn since EnPassant can only occur with pawns
			if (!temp.getPiece().equals("Pawn")) return false;
			/**check if there is a piece underneath the target (X,Y) coordinate:
			 * if there isn't -> then move is automatically illegal
			 * if there is, but the piece is of the same color -> then move is illegal
			 */
			if(temp.player.color.equals("white")){ 
				return false;
			}
			else{//Once this is reached we have checked that the piece under target destination is a black pawn
				Pawn pTemp = (Pawn) temp; //Pawn casting is needed so that we have access to the boolean "doubleMove"
				//returns a valid move if the first and only move made is a double space
				if(pTemp.doubleMove == true && pTemp.moveCount == 1){
					return true;
				}
				else {return false;}
			}
		}
		else{//if player is black; same code as above except for black team
			Piece temp = b1.board[targetX-1][targetY].piece;
			if (temp == null) return false;
			//returns false if the piece is not a pawn since EnPassant can only occur with pawns
			if (!temp.getPiece().equals("Pawn")) return false;
			/**check if there is a piece underneath the target (X,Y) coordinate:
			 * if there isn't -> then move is automatically illegal
			 * if there is, but the piece is of the same color -> then move is illegal
			 */
			if(temp.player.color.equals("black")){ 
				return false;
			}
			else{//Once this is reached we have checked that the piece under target destination is a white pawn
				Pawn pTemp = (Pawn) temp; //Pawn casting is needed so that we have access to the boolean "doubleMove"
				//returns a valid move if the first and only move made is a double space
				if(pTemp.doubleMove == true && pTemp.moveCount == 1){ 
					return true;
				}
				else {return false;}
			}
		}
	}
	
	@Override
	public boolean validMovement2(cord start, cord end, Board b1) {
	
		int startX = start.x;
		int startY = start.y;
		int targetX = end.x;
		int targetY = end.y;
		
		//Check validMove for white pieces:
		if (this.player.color.equals("white")){
			if(targetX == (startX - 1) && targetY == (startY - 1)) return true;// up and to left
			if(targetX == (startX - 1) && targetY == (startY + 1)) return true; // up and to the right
		}
		else{//check for black pieces
			if(targetX == (startX + 1) && targetY == (startY - 1)) return true;// down and to left
			if(targetX == (startX + 1) && targetY == (startY + 1)) return true; // down and to the right
		}
		
		return false;
		
	}

}
