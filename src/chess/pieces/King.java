package chess.pieces;
import chess.Board;
import chess.Piece;
import chess.Player;
import chess.cord;
/**
 * @author andrewgonzalez
 * @author danielrodriguez
 */
public class King extends Piece {
	
	
	public King(){
	}
	
	public King(Player color){
		this.player = color;
	}
	
	

	@Override
	public void Piece(Player color, chess.Piece type) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String getPiece() {
		// TODO Auto-generated method stub
		return "King";
	}

	@Override
	public boolean validMovement(cord start, cord end, Board b1) {
		if(end.x > start.x + 1 || end.x < start.x - 1 
				|| end.y > start.y + 1 || end.y < start.y - 1){
			return false;
		}
		if(b1.board[end.x][end.y].piece != null){
			//friendly fire check
			if(b1.board[end.x][end.y].piece.player.color.equals(this.player.color)){
				return false;
			}
		}
		//this.row = end.x;
		//this.col = end.y;
		moveCount++;
		return true;
	}
	@Override
	public boolean validMovement2(cord start, cord end, Board b1) {
		int startX = start.x;
		int startY = start.y;
		int targetX = end.x;
		int targetY = end.y;
		//Since the king can move in all directions there is no need to differentiate the player's color:
		if(targetX == (startX - 1) && targetY == (startY - 1)) return true;// up and to left
		if(targetX == (startX - 1) && targetY == (startY + 1)) return true; // up and to the right
		if(targetX == (startX - 1) && targetY == (startY)) return true;//straight up
		if(targetX == (startX) && targetY == (startY + 1)) return true;//directly to the right
		if(targetX == (startX) && targetY == (startY - 1)) return true;//directly to the left
		if(targetX == (startX + 1) && targetY == (startY - 1)) return true;// down and to left
		if(targetX == (startX + 1) && targetY == (startY + 1)) return true; // down and to the right
		if(targetX == (startX + 1) && targetY == (startY)) return true;//straight down

		return false;
	}

}
