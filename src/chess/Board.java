package chess;
import chess.pieces.*;
/*
 * Board is made of a 2D array of objects called 'Tiles'. It's 9x9 because it includes the game board 
 * AND the axis labels. 
 * board[row][column]:
 * board[0,0] corresponds to top left tile (a8).
 * board[0,7] = top right tile (h8)
 * board [7,0] & board[7.7]  correspond to a1 & h1 respectively. 
 */
/**
 * @author andrewgonzalez
 * @author danielrodriguez
 */
public class Board {
	public Tile [][] board;
	
	//Board Constructor
	public Board(){
		this.board = new Tile [9][9];
	}
	
	public Player white = new Player("white");
	public Player black = new Player("black");

	
	
	/*populates board*/
	void populateBoard(){
		for(int i = 0; i < 8; i++){
			if(i == 0 || i == 7){
				populatePieces(i);
				continue;
			}
			if(i == 1 || i == 6){
				populatePawns(i);
				continue;
			}
			if(i>= 2 && i<= 5){
				populateMidBoard(i);
				continue;
			}
			
		}
		/*Populates the column all the way at the bottom of grid*/
		for(int i = 0; i < 9; i++){
			if(i == 8){
				for(int k = 0; k < 9; k++){
					Tile testTile = new Tile();
					switch (k){
					case 0 : testTile.xPlane = 'a';
					break;
					case 1 : testTile.xPlane = 'b';
					break;
					case 2 : testTile.xPlane = 'c';
					break;
					case 3 : testTile.xPlane = 'd';
					break;
					case 4 : testTile.xPlane = 'e';
					break;
					case 5 : testTile.xPlane = 'f';
					break;
					case 6 : testTile.xPlane = 'g';
					break;
					case 7 : testTile.xPlane = 'h';
					break;
					case 8 : testTile.xPlane = ' ';
					}
					board[i][k] = testTile;
					
				}
				break;
			}
			/*Populates the column all the way on the right of grid*/
			for(int k = 8; k < 9; k++){
				Tile testTile = new Tile();
				switch (i){
				case 0 : testTile.yPlane = '8';
				break;
				case 1 : testTile.yPlane = '7';
				break;
				case 2 : testTile.yPlane = '6';
				break;
				case 3 : testTile.yPlane = '5';
				break;
				case 4 : testTile.yPlane = '4';
				break;
				case 5 : testTile.yPlane = '3';
				break;
				case 6 : testTile.yPlane = '2';
				break;
				case 7 : testTile.yPlane = '1';
				break;
				case 8 : testTile.yPlane = ' ';
				}
				board[i][k] = testTile;
				
			}
		}
	}
	
	

	/*prints boards*/
	 void printBoard(){
		for(int i = 0; i < 9; i++){
			System.out.println();
			//prints out the letters at the bottom of board
			if(i == 8){
				for(int k = 0; k < 9; k++){
					System.out.print(board[i][k].xPlane + "  ");
				}
				break;
			}
			//second for loop
			for(int k = 0; k < 9; k++){
				//Prints out the numbers all the way on the right
				if (k==8){
					System.out.print(board[i][k].yPlane + " ");
				}
				//Prints out the contents of board
				else{
					if(board[i][k].piece == null){
						System.out.print(board[i][k].symbol +" ");
					}
					else System.out.print(board[i][k].piece.pieceAbriv + " ");
//					if(board[i][k].symbol.equals("  ") || board[i][k].symbol.equals("##") ){
//						System.out.print(board[i][k].symbol +" ");
//					}
//					else{
//					System.out.print(board[i][k].piece.pieceAbriv + " ");
//					}
				}
			}
		}
	}
	 
	 /** 
	  * Populates pawns given the row x.
	  * @param x
	  */
	void populatePawns(int row){
		//populate black pawns
		if(row == 1){
			for(int i=0;i<8;i++){
				Pawn bPawn = new Pawn(black);
				bPawn.pieceAbriv = "bp";
				bPawn.row = row;
				bPawn.col = i;
				Tile tile = new Tile(bPawn);
				//If determines the color/symbol of the tile ('#' or blank)
				if (i%2 == 0) tile.symbol = "##";
				
				board[row][i] = tile;
			}
		}
		
		//populates white pawns
		else{
			for(int i=0;i<8;i++){
				Pawn wPawn = new Pawn(white);
				wPawn.pieceAbriv = "wp";
				wPawn.row = row;
				wPawn.col = i;
				Tile tile = new Tile(wPawn);
				//If determines the color/symbol of the tile ('#' or blank)
				if (i%2 == 1) tile.symbol = "##";
				board[row][i] = tile;
			}
		}
		
	}
	
	/** 
	  * Populates pieces given the row x.
	  * @param x
	  */
	void populatePieces(int row){
		//black pieces
		if(row == 0){
			for(int i=0;i<8;i++){
				switch (i){
				case 0 : Rook rook = new Rook(black);//Rook
				rook.pieceAbriv = "bR";
				rook.row = row;
				rook.col = i;
				Tile tile = new Tile(rook);
				board[row][i] = tile;
				break;
				case 1 : Knight knight = new Knight(black);//Knight
				knight.pieceAbriv = "bN";
				knight.row = row;
				knight.col = i;
				Tile tile1 = new Tile(knight);
				tile1.symbol = "##";
				board[row][i] = tile1;
				break;
				case 2 : Bishop bishop = new Bishop(black);//Bishop
				bishop.pieceAbriv = "bB";
				bishop.col = i;
				bishop.row = row;
				Tile tile2 = new Tile(bishop);
				board[row][i] = tile2;
				break;
				case 3 : Queen queen = new Queen(black);//Queen
				queen.pieceAbriv = "bQ";
				queen.col = i;
				queen.row = row;
				Tile tile3 = new Tile(queen);
				tile3.symbol = "##";
				board[row][i] = tile3;
				break;
				case 4 : King king = new King(black) ;//King
				king.pieceAbriv = "bK";
				king.col = i;
				king.row = row;
				Tile tile4 = new Tile(king);
				board[row][i] = tile4;
				break;
				case 5 : Bishop bishop1 = new Bishop(black);//Bishop
				bishop1.pieceAbriv = "bB";
				bishop1.col = i;
				bishop1.row = row;
				Tile tile5 = new Tile(bishop1);
				tile5.symbol = "##";
				board[row][i] = tile5;
				break;
				case 6 : Knight knight1 = new Knight(black);//Knight
				knight1.pieceAbriv = "bN";
				knight1.col = i;
				knight1.row = row;
				Tile tile6 = new Tile(knight1);
				board[row][i] = tile6;
				break;
				case 7 : Rook rook1 = new Rook(black);//Rook
				rook1.pieceAbriv = "bR";
				rook1.col = i;
				rook1.row = row;
				Tile tile7 = new Tile(rook1);
				tile7.symbol = "##";
				board[row][i] = tile7;
				break;
				}
				
				
			}
			
		}
		//Populates white pieces
		else{
				for(int i=0;i<8;i++){
					switch (i){
					case 0 : Rook rook = new Rook(white);//Rook
					rook.pieceAbriv = "wR";
					rook.row = row;
					rook.col = i;
					Tile tile = new Tile(rook);
					tile.symbol = "##";
					board[row][i] = tile;
					break;
					case 1 : Knight knight = new Knight(white);//Knight
					knight.pieceAbriv = "wN";
					knight.row = row;
					knight.col = i;
					Tile tile1 = new Tile(knight);
					board[row][i] = tile1;
					break;
					case 2 : Bishop bishop = new Bishop(white);//Bishop
					bishop.pieceAbriv = "wB";
					bishop.row = row;
					bishop.col = i;
					Tile tile2 = new Tile(bishop);
					tile2.symbol = "##";
					board[row][i] = tile2;
					break;
					case 3 : Queen queen = new Queen(white);//Queen
					queen.pieceAbriv = "wQ";
					queen.row = row;
					queen.col = i;
					Tile tile3 = new Tile(queen);
					board[row][i] = tile3;
					break;
					case 4 : King king = new King(white) ;//King
					king.pieceAbriv = "wK";
					king.row = row;
					king.col = i;
					Tile tile4 = new Tile(king);
					tile4.symbol = "##";
					board[row][i] = tile4;
					break;
					case 5 : Bishop bishop1 = new Bishop(white);//Bishop
					bishop1.pieceAbriv = "wB";
					bishop1.row = row;
					bishop1.col = i;
					Tile tile5 = new Tile(bishop1);
					board[row][i] = tile5;
					break;
					case 6 : Knight knight1 = new Knight(white);//Knight
					knight1.pieceAbriv = "wN";
					knight1.row = row;
					knight1.col = i;
					Tile tile6 = new Tile(knight1);
					tile6.symbol = "##";
					board[row][i] = tile6;
					break;
					case 7 : Rook rook1 = new Rook(white);//Rook
					rook1.pieceAbriv = "wR";
					rook1.row = row;
					rook1.col = i;
					Tile tile7 = new Tile(rook1);
					board[row][i] = tile7;
					break;
					}
				}
		}
		
	}
	
	/**
	 * Populates middle of board with "##" and '  '
	 */
	void populateMidBoard(int row){
		//even rows
		if(row%2 == 0){
			for(int i=0;i<8;i++){
				if(i%2 != 0){
					Tile tile = new Tile();
					tile.symbol = "##";
					board[row][i] = tile;
				}
				else{
					Tile tile = new Tile();
					tile.symbol = "  ";
					board[row][i] = tile;
				}
			}
		}
		else{
			for(int i=0;i<8;i++){
				if(i%2 == 0){
					Tile tile = new Tile();
					tile.symbol = "##";
					board[row][i] = tile;
				}
				else{
					Tile tile = new Tile();
					tile.symbol = "  ";
					board[row][i] = tile;
				}
			}
		}
		
	}

}
