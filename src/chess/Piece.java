package chess;
/**
 * @author andrewgonzalez
 * @author danielrodriguez
 */
public abstract class Piece {
	/** Abstract class Piece is used a blueprint for all chess pieces**/

	public Player player;
	public Piece type;
	public String pieceAbriv;
	public int moveCount = 0; //used for EnPassant and Castling
	
	public int row;
	public int col;
	public int index; //used for arraylist index
	public boolean validMovement = false;
	/**
	 *initializes the Piece with team, and type (pawn,knight,etc).
	 * @param color
	 * @param type
	 */
	public abstract void Piece (Player color, Piece type);
	
	
	
	/**
	 * checks if the piece at the given start coordinate(tile)
	 * can validly move to the desired end coordinate(tile).
	 * @param start
	 * @param end
	 * @return
	 */
	//public abstract boolean validMovement(cord start, cord end);
	public abstract boolean validMovement(cord start, cord end, Board b1);
	public abstract boolean validMovement2(cord start, cord end, Board b1);
	
	
	// returns the piece type.
	public abstract String getPiece();
	
	//overriding equals method, may not be necessary 
		public boolean equals(Object o){
			if(o == null || !(o instanceof Piece)){
				return false;
			}
			Piece other = (Piece)o;
			return type == other.type || player == other.player;
		}
}
