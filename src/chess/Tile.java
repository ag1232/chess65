package chess;
/*
 * Used to construct the board.
 */
/**
 * @author andrewgonzalez
 * @author danielrodriguez
 */
public class Tile {
	/**Piece is to determine:
	 * 1)If the tile is empty.
	 * 2) If it is not empty, which piece occupies the tile.
	 * 
	 */
	public Piece piece = null;
	
	//used for GUI purposes only, to display # and " "
	public String symbol = "  ";
	
	
	//Used for x-axis letters and y-axis numbers
	public char xPlane;
	public char yPlane;
	
	
	
	/* Needs 2 constructors:
	 * 1) With just x and y coordinates
	 * 2) With x, y, coords and the Piece which is going to be placed on the tile.
	 * 3) With just char xPlane and int Yplane for UI coordinate purpose.
	 */
	public Tile(Piece type){
		this.piece = type;
	}
	
	public Tile(){}
	
	
	
	/**
	 * First checks whether or not the tile is empty
	 * at the given (x,y).
	 * @return
	 */
	public boolean isEmpty(){
		 if (piece ==  null){
			 return true;
		 }
		 else{
			 
			 return false;
		 }
	}
	/**
	 * If the tile IS NOT empty the following method will
	 * check the current piece's color which is used to 
	 * determine if the movement is valid:
	 * if currentPieceColor = occupyingTilePiece -> Invalid move
	 * (same colored pieces can't capture each other).
	 * else move is valid (black caps white, and vice versa).
	 */
	public String getOccupyingPieceColor(){
		return piece.player.color;
	}

}
